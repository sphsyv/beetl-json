﻿
Beetl-JsonTool 作为beetl模板引擎的一个附属工具，提供了对象序列化成json技术。其原理是基于(Location:action)*
功能强大，扩展性强，而体积小，仅仅不到42K。

##Location
定义了一个序列化的位置，比如最简单的location就是属性名称，或者是属性类型,如

* nane:nn/myName/ 遇到name属性，输出的是myName，nn是action指示，表示New Name
* ~d:f/yyyy-MM-dd/ 遇到类型为日期的，格式化输出。～表示属性名是类型，d是Java.Util.Date的简称。
    * ~f 表示一个number类型
    * ~L 表示后面是个具体类，如{~L/java.util.Date/:f/yyyy-MM-dd/，如果具体类名后有×，表示其子类也匹配
    * ~c 表示是一个集合
    * ~d 表示一个日期
* ~*:I/id,name/  *代表类，I动作是忽略此类属性id,name

 


##Action
定义了一个匹配动作的输出，有以下几种

* i:表示忽略此属性，如{name:i} 遇到name属性，忽略
* O:表示排序，如{~*:O/name, age/} 。name和age属性总是排在最前面
* I:表示忽略类的属性，如{~*:I/name, age,bir/} 
* Ci:出现循环引用，仅仅显示参数指定的字段，如{~*:Ci/name,id/} ，该类实例被循环引用了，只显示name，和id。其他字段不显示。
* nn 表示更改名字，{nane:nn/myName/}  将输出myName
* ? 代表一个if，如果满足条件，则指定一个输出，如{xxList:?null->[]},如果某集合xxList 为null，则输出一个[]
，?后年可以指定null，empty，以及某个数字。->表示输出
* $.xxx，用于调用当前位置对象的某个方法，如{ts:$.get},并不是输出ts,而是输出ts.get(). 调用后，可以使用->在使用一个Action，比如：
"~L/java.util.Calendar*/:$.getTime->f/yyyy-MM-dd/" 遇到Calendar和其子类，先获取Date类型，然后格式化
* f 用于格式化输出，如number:f/#.##/,也可以自定义格式化函数并注册，比如注册一个格式化函数Money,
则number:fMoney 则会调用money做输出

##例子:

    //全局设定
	JsonTool.addLocationAction("~d","f/yyyy.MM.dd/");	
	JsonTool.addLocationAction("~L/java.util.Calendar*/","$.getTime->f/yyyy-MM-dd/");
	//类json格式的策略，用逗号分开多个locationAction
    JsonTool.addPolicy("~f:f/#.##/,~c:?null->[]");
	// 默认是紧凑输出，使用true，将换行和缩进	
	JsonTool.pretty = true;
	//序列化User
	String json = JsonTool.serialize(User);
	//or  指定一个序列化策略,age，name先输出，适合有特殊需求的对象或者无法注解（第三方）对象
    String json2 = JsonTool.serialize(User，"~*:O/age,name/"));
 

User 定义

	@Json(
		policys={
				@JsonPolicy(location="name", action="nn/newUserName/"),
				@JsonPolicy(location="deleteList", action="?empty->[]")						
		}
	)
	public class User{	
		String name="joel";
		int age =12;	
		double salary=12.32266;
		Customer  customer = new Customer();
		List<Customer>  list = new ArrayList<Customer>();
		List<Customer>  deleteList = null;
		//getter and setter 	方法必须有，在此忽略
	}

Customer定义

	@Json(
		policys={
				@JsonPolicy(location="name", action="nn/userName/")			
		}
	)
	class Customer{
		String name="lijz";
		int age=11;
		Date bir = new Date();
		//getter and setter 	方法必须有，在此忽略
	}


## 主要api：

* JsonTool.addLocationAction(String loc,String action)   :增加一个全局的序列化策略，如JsonTool.addLocationAction("~d","f/yyyy.MM.dd/");	
* JsonTool.pretty:默认是紧凑输出，改为true，则有缩进和换行
* JsonTool.serialize(Object o) :序列化对象，返回String
* JsonTool.serialize(Object o,Writer w) :序列化对象到Writer里
* JsonTool.serialize(Object o,String policy) :序列化对象，返回String,并使用额外的序列化策略
* JsonTool.serialize(Object o,Writer w,String policy) :序列化对象到Writer里,并使用额外的序列化策略


## 下一步：

* spring集成
* 如果现在的循环处理不能满足需求，可以采用循环引用,输出上次完整输入的路径地址，以方便通过js代码获取到此循环引用。不过我感觉现有方式应该够了
* 性能优化，争取前三
* 反序列化，总体大小不超过100k。