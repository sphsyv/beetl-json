package org.beetl.json.performance;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.beetl.json.JsonTool;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SimpleDateFormatSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestSimple {
	public static void main(String[] args){
		testSimple();
//		testList();
		
	}
	
	public static void testList(){
		int max = 1000000;
		List fastList = getFastJsonData();
		List beetList = getBeetlJsonData();
		SerializeConfig mapping = new SerializeConfig();
        mapping.setAsmEnable(false);
        String dateFormat = "yyyy-MM-dd";  
        mapping.put(Date.class, new SimpleDateFormatSerializer(dateFormat));
        TimeLog.key2Start();
		for(int i=0;i<max;i++){
			String json = JSON.toJSONString(fastList,mapping);
		}
		TimeLog.key2End();
		
		JsonTool.addLocationAction("~d", "f/yyyy-MM-dd/");
		TimeLog.key1Start();
		for(int i=0;i<max;i++){
			String json = JsonTool.serialize(beetList);
		}
		TimeLog.key1End();
		
		
		TimeLog.display("beetl-json","fastjson");
	}
	
	private static List<Object> getBeetlJsonData(){
		List list = new ArrayList();
		for(int i=0;i<10;i++){
			list.add(new SimpleObject());
		}
		return list;
	}
	
	private static List<Object> getFastJsonData(){
		List list = new ArrayList();
		for(int i=0;i<10;i++){
			list.add(new SimpleObject());
		}
		return list;
	}
	
	
	public static void testSimple(){
		SimpleObject so = new SimpleObject();
		SimpleObject fso = new SimpleObject();
		int max = 1000000;
		SerializeConfig mapping = new SerializeConfig();
        mapping.setAsmEnable(false);
        String dateFormat = "yyyy-MM-dd";  
        mapping.put(Date.class, new SimpleDateFormatSerializer(dateFormat));
        TimeLog.key2Start();
		for(int i=0;i<max;i++){
			String json = JSON.toJSONString(fso,mapping);
		}
		TimeLog.key2End();
		
		JsonTool.addLocationAction("~d", "f/yyyy-MM-dd/");
		TimeLog.key1Start();
		for(int i=0;i<max;i++){
			String json = JsonTool.serialize(so);
		}
		TimeLog.key1End();
		
		ObjectMapper objectMapper = new ObjectMapper();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		objectMapper.setDateFormat(sdf);
		
		
		 TimeLog.key3Start();
		for(int i=0;i<max;i++){
			StringWriter sw = new StringWriter();
			try {
				objectMapper.writeValue(sw, so);
				String json = sw.toString();
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		TimeLog.key3End();
		
		
		
		TimeLog.display("beetl-json","fastjson","jackson");
	}
	
	
	


}
