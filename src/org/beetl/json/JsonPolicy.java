package org.beetl.json;

public @interface JsonPolicy {
	String location();
	String action();
}
