package org.beetl.json.node;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import org.beetl.json.ActionComparator;
import org.beetl.json.ActionReturn;
import org.beetl.json.JsonTool;
import org.beetl.json.JsonWriter;
import org.beetl.json.Location;
import org.beetl.json.LocationAction;
import org.beetl.json.OutputNode;
import org.beetl.json.Policy;
import org.beetl.json.action.IValueAction;

public class IterableClassNode implements OutputNode {
	protected  TreeSet<IValueAction>  valueActions =  new TreeSet(new ActionComparator<IValueAction>());
	
	@Override
	public void render(Object obj, JsonWriter w) throws IOException {
		Object attrValue = obj;
		if(valueActions.size()==0){
			NodeUtil.writeIterator (this, ((Iterable)obj).iterator(), w);
			
		}else{
			NodeUtil.writeUnkonw(this, obj, w, valueActions);
		}
	}

	@Override
	public void setActionByLocation(List<Location> list) {
		for(Location l:list){			
			LocationAction action = l.getAction();
				 if(action instanceof IValueAction){		
					 if(isMatch(l)){						 
						 this.valueActions.add( (IValueAction)action);
					 }					
				}
		}
	}
	
	protected boolean isMatch(Location location) {
		return location.match(Iterable.class,null);
	}

}
