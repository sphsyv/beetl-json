package org.beetl.json.node;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import org.beetl.json.ActionComparator;
import org.beetl.json.ActionReturn;
import org.beetl.json.JsonTool;
import org.beetl.json.JsonWriter;
import org.beetl.json.Location;
import org.beetl.json.LocationAction;
import org.beetl.json.OutputNode;
import org.beetl.json.Policy;
import org.beetl.json.action.IValueAction;

public class ArrayNode extends  IterableClassNode {
	
	@Override
	public void render(Object obj, JsonWriter w) throws IOException {	
		if(valueActions.size()==0){
			NodeUtil.writeIterator(this, Arrays.asList((Object[])obj).iterator(), w);
		}else{
			NodeUtil.writeUnkonw(this, obj, w, valueActions);
		}
		
	}

	
	

}
