package org.beetl.json.node;

import java.util.Map;

import org.beetl.json.Location;

public class MapClassNode extends IterableClassNode {
	
	
	
	protected boolean isMatch(Location location) {
		return location.match(Map.class,null);
	}

}
