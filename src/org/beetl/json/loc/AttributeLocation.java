package org.beetl.json.loc;

import org.beetl.json.Location;

public class AttributeLocation extends Location {

	String attrName ;
	public AttributeLocation(String attrName){
		this.attrName = attrName;
	}
	@Override
	public boolean match(Class type, String field) {
		if(field.equals(attrName)){
			return true;
		}
		return false;
	}
}
