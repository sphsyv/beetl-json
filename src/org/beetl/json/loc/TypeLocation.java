package org.beetl.json.loc;


import java.util.Collection;
import java.util.Date;

import org.beetl.json.Location;

/**内置类型
 * @author xiandafu
 *
 */
public class TypeLocation extends Location {

	String type = "";
	public TypeLocation(String type){
		this.type = type;
	}
	@Override
	public boolean match(Class cls, String field) {
		if(type.equals("f")){
			if(cls==Double.class||cls==double.class){
				return true;
			}else{
				return false;
			}
		}else if(type.equals("d")){
			if(Date.class.isAssignableFrom(cls)){
				return true;
			}else{
				return false;
			}
		}else if(type.equals("c")){
			if(Iterable.class.isAssignableFrom(cls)){
				return true ;
			}else{
				return false ;
			}
		}else if(type.equals(cls.getName())){
			return true;
		}
		else{
			return false;
		}
	
	}

}
