package org.beetl.json.loc;

import org.beetl.json.Location;

public class ClassLocation extends Location {

	Class c  = null;
	public ClassLocation(Class c){
		this.c = c ;
	}
	@Override
	public boolean match(Class type, String field) {
		return  c==type;
	}

}
