package org.beetl.json.loc;


import java.util.Collection;
import java.util.Date;

import org.beetl.json.Location;

/**指定类型
 * @author xiandafu
 *
 */
public class ClassTypeLocation extends Location {

	Class target = null;
	boolean exactMatch = false ;
	public ClassTypeLocation(Class target,boolean exactMatch){
		this.target = target;
		exactMatch = true;
	}
	@Override
	public boolean match(Class cls, String field) {
		if(exactMatch){
			return target==cls;
		}else{
			return target.isAssignableFrom(cls);
		}
	
	}

}
