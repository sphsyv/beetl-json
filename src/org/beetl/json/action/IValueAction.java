package org.beetl.json.action;

import org.beetl.json.ActionReturn;
import org.beetl.json.LocationAction;
import org.beetl.json.OutputNode;

public interface IValueAction extends LocationAction  {
	public ActionReturn doit(Object o,OutputNode thisNode);
}
