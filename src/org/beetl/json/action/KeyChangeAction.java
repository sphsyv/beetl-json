package org.beetl.json.action;

import org.beetl.json.ActionReturn;
import org.beetl.json.OutputNode;

public class KeyChangeAction implements IKeyAction {

	String targetName;
	public KeyChangeAction(String targetName){
		this.targetName = targetName;
	}
	@Override
	public ActionReturn doit(Object o, OutputNode thisNode) {
		// TODO Auto-generated method stub
		return new ActionReturn(targetName);
	}
	@Override
	public int getIndex() {
		// TODO Auto-generated method stub
		return Integer.MAX_VALUE;
	}

}
