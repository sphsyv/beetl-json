package org.beetl.json.action;

import org.beetl.json.ActionReturn;
import org.beetl.json.LocationAction;
import org.beetl.json.OutputNode;

public interface IClassAction extends LocationAction  {
	public ActionReturn doit(OutputNode thisNode);
}
