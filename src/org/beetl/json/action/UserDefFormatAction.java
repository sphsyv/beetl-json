package org.beetl.json.action;

import org.beetl.json.ActionReturn;
import org.beetl.json.Format;
import org.beetl.json.JsonTool;
import org.beetl.json.OutputNode;

public class UserDefFormatAction implements IValueAction {

	Format ft = null;
	public UserDefFormatAction(String name){
		ft = JsonTool.getFormat(name);
	}
	
	@Override
	public int getIndex() {
		// TODO Auto-generated method stub
		return Integer.MAX_VALUE;
	}

	@Override
	public ActionReturn doit(Object o, OutputNode thisNode) {
		Object value = ft.format(o);
		return new ActionReturn(value,false);
	}

}
