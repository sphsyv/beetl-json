package org.beetl.json.action;

import org.beetl.json.ActionReturn;
import org.beetl.json.OutputNode;

/**忽略
 * @author xiandafu
 *
 */
public class KeyIgnoreAction implements IKeyAction {

	@Override
	public ActionReturn doit(Object o,OutputNode thisNode){
		//todo:can reuse 
		return new ActionReturn(null,false);
	}

	@Override
	public int getIndex() {
		//总是先执行
		return 0;
	}

}
