package org.beetl.json;

import java.io.IOException;
import java.io.Writer;

import org.beetl.json.util.NoLockStringWriter;

public class Policy {
	public OutputNode  classNode ;
	public Policy(OutputNode classNode){
		this.classNode = classNode;
	}
	public String toJson(Object obj)throws IOException {
		NoLockStringWriter writer = new NoLockStringWriter();
		toJson(writer,obj);		;
		String json =  writer.toString();
		
		return json;
	}
	
	public void toJson(Writer writer,Object obj) throws IOException{
		
		classNode.render(obj, new JsonWriter(writer));
	}
	
public void toJson(JsonWriter writer,Object obj) throws IOException{
		
		classNode.render(obj, (writer));
	}
}
