package org.beetl.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test {

	public static void main(String[] args)throws IOException {
		{
			
			{
//				String policy= "~c:?null->[],~f:f/#.##/";
//				List<Location> list = JsonTool.parseStringPolicy(policy);
//				for(Location loc:list){
//					System.out.println(loc.getClass());
//				}
			}
			
			JsonTool.addLocationAction("~d","f/yyyy.MM.dd/");	
			JsonTool.addPolicy("~f:f/#.##/,~c:?null->[]");
	
//			JsonTool.addLocationAction("~L/java.util.Calendar*/","$.getTime->f/yyyy-MM-dd/");
			
			JsonTool.pretty = false;
			
			{
					
//				User user = new User();		
//				String jsonString = JsonTool.serialize(user);			
////				System.out.println("ojbString="+ojbString);
//				System.out.println("jsonString="+jsonString);
			}
			
			{
				Foo foo = new Foo();
				String str = JsonTool.serialize(foo);
				System.out.println(str);
				str = JsonTool.serialize(foo,"~L/java.util.Calendar*/:$.getTime->f/yyyy-MM-dd/");
				System.out.println(str);
			}
			{
//				Policy policy = JsonTool.getDefaultPolicy(Map.class);		
//				Map map = new HashMap();
//				map.put("date",Calendar.getInstance());
//				map.put("age",1);
//				map.put("name","joelli");
//				map.put("array", new Object[]{1,2,"abc"});
//				String json = policy.toJson(map);
//				System.out.println("jsonString="+json);
			}
			
			{
//				Policy policy = JsonTool.getDefaultPolicy(List.class);		
//				List list = new ArrayList();
//				list.add(Calendar.getInstance());
//				list.add(1);
//				list.add("abc");
//				String json = policy.toJson(list);
//				System.out.println("jsonString="+json);
			}
			
//			Policy policy = JsonTool.getDefaultPolicy(Foo.class);	
//			
//			String jsonString = policy.toJson(new Foo());
//		
//			System.out.println("jsonString="+jsonString);
		
		}
	}	

}



class Foo{

	Calendar  calendar = Calendar.getInstance();

	public Calendar getCalendar() {
		return calendar;
	}

	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}
	
	

	
	
}


@Json(
		policys={
				@JsonPolicy(location="name", action="nn/newUserName/"),
				@JsonPolicy(location="~*", action="O/customer,id/")	,
				@JsonPolicy(location="~*", action="Ci/id/")	
		}
)
class User{	
	int id = 1;
	Customer  customer  = null;	
	public User(){
		customer = new Customer(this);	
	}
	
	
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}

@Json(
		policys={
				@JsonPolicy(location="name", action="nn/userName/")			
		}
)
class Customer{
	String name="lijz";
	
	User user = null;
	public Customer(User user){
		this.user = user;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	
	
	
}
