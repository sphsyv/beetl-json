package org.beetl.json;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述了序列化规则一个loc:action
 * @author xiandafu
 *
 */
public abstract class Location {
	Location parent = null;
	String locName = "";
	List<Location> children = new ArrayList<Location>();
	LocationAction action = null;	
	public Location getParent(){
		return parent;
	}

	
	public LocationAction getAction() {
		return action;
	}
	public void setAction(LocationAction action) {
		this.action = action;
	}
	
	public void addLocation(Location loc){
		if(children==null){
			children = new ArrayList<Location>();
		}
		children.add(loc);
	}
	public abstract boolean match(Class type, String field);

	public List<Location> getChildren() {
		return children;
	}

	public void setChildren(List<Location> children) {
		this.children = children;
	}


	public String getLocName() {
		return locName;
	}


	public void setLocName(String locName) {
		this.locName = locName;
	}
	
	
}
