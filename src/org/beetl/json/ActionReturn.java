package org.beetl.json;

public class ActionReturn {
	public Object value;
	public boolean process;
	public ActionReturn(Object value,boolean process){
		this.process = process;
		this.value = value;
	}
	
	public ActionReturn(Object value){
		this.process = true;
		this.value = value;
	}
}
