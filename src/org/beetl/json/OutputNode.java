package org.beetl.json;

import java.io.IOException;
import java.io.Writer;
import java.util.List;


/** primitiv ,list .obj,etc
 * @author xiandafu
 *
 */
public interface  OutputNode {	
	public  void render(Object obj,JsonWriter w) throws IOException;
	public void  setActionByLocation(List<Location> list);


	
}
